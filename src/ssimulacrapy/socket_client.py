import os
import json

from ssimulacrapy.options import args

# lock behind --host so the user doesn't need to install websocket if they don't want to.
if args.host:
    from typing import Any

    from websocket import create_connection
    from websocket._core import WebSocket


class SocketClient:
    def __init__(self, host: str) -> None:
        self.host: str = host

    def socket_client(self, payload: dict, host: str) -> None:
        """Send dictionary to websocket server as JSON

        Args:
            payload (dict): Dictionary to send
            host (str): Websocket host (IP) of server to send to in the format ``ws://host`` or ``wss://host``
        """

        # This is adequate for now. (Maybe..)
        ws: WebSocket = create_connection(host)
        ws.send(json.dumps(payload).encode() + b"\n")
        ws.close()

    def start(
        self,
        abs_source_path: str,
        start_frame: int,
        end_frame: int,
        abs_encoded_path: list,
        encoded_clip_hash: dict,
        metric: str,
    ) -> None:
        """Tell the server we're starting"""

        socket_payload: dict[str, Any] = {
            "start": True,  # tell the server we are starting. Not sure how helpful this is.
            "source": os.path.basename(abs_source_path),
            "encoded": [],
            "start_frame": start_frame,
            "end_frame": end_frame,
            "metric": metric,
        }

        # add the encoded files
        for encoded_file in abs_encoded_path:
            file_hash: str = encoded_clip_hash[os.path.basename(encoded_file)]
            dictionary: dict = {file_hash: os.path.basename(encoded_file)}
            socket_payload["encoded"].append(dictionary)

        # send the payload
        self.socket_client(socket_payload, self.host)

    def send_frame(
        self,
        encoded_clip_hash: dict,
        encoded_file: str,
        frame: int,
        total_frames: int,
        score: float,
    ) -> None:
        """Send a frame to the server

        Args:
            encoded_clip_hash (dict): The hash of the encoded clips
            encoded_file (str): The encoded file
            frame (int): The frame number
            total_frames (int): The total number of frames
            score (float): The score
        """
        socket_payload: dict[str, Any] = {
            "encoded": encoded_clip_hash[os.path.basename(encoded_file)],
            "file": os.path.basename(encoded_file),
            "frame": frame,
            "total_frames": total_frames,
            "score": score,
        }

        self.socket_client(socket_payload, self.host)

    def send_finish(self, final_score: dict) -> None:
        """Tell the server we're done and send the final scores"""

        final_score["end"] = True
        socket_payload: dict = final_score

        self.socket_client(socket_payload, self.host)
