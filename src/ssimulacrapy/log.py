from loguru import logger
from rich.console import Console

console = Console()
logger.remove()


def _log_formatter(record: dict) -> str:
    """Log message formatter"""
    color_map = {
        "TRACE": "dim blue",
        "DEBUG": "bold purple",
        "INFO": "bold blue",
        "SUCCESS": "bold green",
        "WARNING": "bold yellow",
        "ERROR": "bold red",
        "CRITICAL": "bold red",
    }
    lvl_color = color_map.get(record["level"].name, "cyan")

    # default format
    format = f"[{lvl_color}]{{level}}[/{lvl_color}]" + f": [bold]{{message}}[/bold]"

    # add extra space after the level to make it even with critical
    if record["level"].name == "WARNING":
        format = (
            f"[{lvl_color}]{{level}}[/{lvl_color}]" + f":  [bold]{{message}}[/bold]"
        )

    if record["level"].name == "INFO":
        format = (
            f"[{lvl_color}]{{level}}[/{lvl_color}]" + f":     [bold]{{message}}[/bold]"
        )

    return format


logger.add(
    console.print,
    level="TRACE",
    format=_log_formatter,
    colorize=True,
)
