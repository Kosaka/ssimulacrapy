import os
import json
from sys import exit

from ssimulacrapy.log import logger as log
from ssimulacrapy.utils import calculate_scores
from ssimulacrapy.options import MetricImplementation, args


class Scores:
    def __init__(self, scores_path: str) -> None:
        self.scores_path: str = scores_path
        self.scores: dict = {
            "source": {},
        }

    def add_video(
        self,
        distorted_files: list[str],
        distorted_clip_hash: dict[str, str],
        source_file: str,
    ) -> None:
        """Add a video to the scores file"""
        if source_file not in self.scores["source"]:
            self.scores["source"][source_file] = {"encoded": {}}

        for encoded_file in distorted_files:
            encoded_filename = os.path.basename(encoded_file)
            clip_hash = distorted_clip_hash[encoded_filename]

            if clip_hash not in self.scores["source"][source_file]["encoded"]:
                self.scores["source"][source_file]["encoded"][clip_hash] = {
                    "file": encoded_filename,
                    "scores": {"frame": {}, "final": {}},
                    # "frames": 0,
                    "start_frame": args.start,
                    "end_frame": args.end,
                    "every": args.every,
                }

    def add_frame(
        self,
        source_file: str,
        clip_hash: str,
        frame: int,
        score: dict,
    ) -> None:
        """Add a frame to the scores file"""
        frame_entry = self.scores["source"][source_file]["encoded"][clip_hash][
            "scores"
        ]["frame"].setdefault(str(frame), {})

        # Handle nested metric structure
        def process_scores(scores, entry):
            for key, value in scores.items():
                if isinstance(value, dict):
                    if key not in entry:
                        entry[key] = {}
                    process_scores(value, entry[key])
                else:
                    entry[key] = float(str(value)[:6])

        process_scores(score, frame_entry)

    def exists(self, source_file: str, clip_hash: str, metric: str) -> bool:
        """Check if a metric exists for a video"""
        metric_impl = MetricImplementation.get(metric)

        output = (
            source_file in self.scores["source"]
            and clip_hash in self.scores["source"][source_file]["encoded"]
            and metric
            in self.scores["source"][source_file]["encoded"][clip_hash]["scores"][
                "final"
            ].get(metric_impl.actual_metric, {})
        )
        return output

    def get_frames(self, source_file: str, distorted_clip_hash: str) -> int:
        """Get the number of frames in a distorted video"""
        return len(
            self.scores["source"][source_file]["encoded"][distorted_clip_hash][
                "scores"
            ]["frame"].values()
        )

    def get_scores(self, source_file: str, distorted_clip_hash: str) -> list[dict]:
        """Get the scores for a video"""
        return list(
            self.scores["source"][source_file]["encoded"][distorted_clip_hash][
                "scores"
            ]["frame"].values()
        )

    def get_final_scores(self, source_file: str, distorted_clip_hash: str) -> dict:
        """Get the scores for a video"""
        return self.scores["source"][source_file]["encoded"][distorted_clip_hash][
            "scores"
        ]["final"]

    def finalise_video(
        self,
        source_file: str,
        encoded_file: str,
        distorted_clip_hash: dict[str, str],
    ) -> None:
        """Calculate and store final scores for each metric"""
        clip_data = self.scores["source"][source_file]["encoded"][
            distorted_clip_hash[encoded_file]
        ]

        # Calculate final scores from frame data
        frame_scores = clip_data["scores"]["frame"].values()
        final_scores = calculate_scores(frame_scores)
        clip_data["scores"]["final"] = final_scores

    def load(self) -> None:
        """Load the scores file"""
        try:
            with open(self.scores_path, "r") as f:
                self.scores = json.load(f)

        except FileNotFoundError:
            log.critical("\nScores file doesn't exist. Exiting...")
            exit(1)

        except json.decoder.JSONDecodeError:
            log.critical(
                "\nScores file is not valid JSON. File may have been edited while the program was running. Exiting..."
            )
            exit(1)

    def save(self) -> None:
        """Save the scores file"""
        try:
            with open(self.scores_path, "w") as f:
                json.dump(self.scores, f)
                f.write("\n")

        except FileNotFoundError:
            log.critical("\nScores file path doesn't exist. Exiting...")
            exit(1)

        except PermissionError:
            log.critical("\nCannot write to scores file. Exiting...")
            exit(1)
