from rich.progress import (
    Progress,
    BarColumn,
    TextColumn,
    SpinnerColumn,
    ProgressColumn,
    TimeElapsedColumn,
    MofNCompleteColumn,
    TaskProgressColumn,
    TimeRemainingColumn,
)

from ssimulacrapy.log import console
from ssimulacrapy.options import MetricImplementation, args


def can_show_avg_score() -> bool:
    # check if the metric has multiple sub metrics
    metric_impl = MetricImplementation.get(args.metric)
    return metric_impl.sub_metrics is None


# Rich's progress bar has an issue where if you try to mess with the task.speed to make it not spit out a billion characters it will error out.
# So we change the class here to make it work.
class TaskSpeed(ProgressColumn):
    """Custom TaskSpeedColumn to work with Rich's progress bar"""

    def render(self, task):
        return f"{( task.speed or 0 ):>3.0f} fps"


progress = Progress(
    SpinnerColumn("dots"),
    TextColumn("{task.description}", style="bold"),
    BarColumn(complete_style="green"),
    TaskProgressColumn(text_format="[progress.percentage]{task.percentage:>3.0f}%"),
    MofNCompleteColumn(),
    TaskSpeed(),
    TimeElapsedColumn(),
    "ETA:",
    TimeRemainingColumn(),
    (
        TextColumn("AVG: Score: {task.fields[score]}", style="bold")
        if can_show_avg_score()
        else TextColumn("")
    ),
    console=console,
)
