import os
import sys
import json
import argparse
import subprocess

import vapoursynth

args = argparse.ArgumentParser()
args.add_argument(
    "--source",
    type=str,
)
args.add_argument(
    "--encoded",
    type=str,
)
args.add_argument(
    "--height",
    type=int,
)
args.add_argument(
    "--width",
    type=int,
)
args.add_argument(
    "--skip-frames",
    type=int,
)
args.add_argument(
    "--start",
    type=int,
)
args.add_argument(
    "--end",
    type=int,
)
args.add_argument(
    "--threads",
    dest="threads",
    type=int,
)
args.add_argument(
    "--source-importer",
    type=str,
)
args.add_argument(
    "--encoded-importer",
    type=str,
)
args.add_argument(
    "--available-importers",
    type=str,
    nargs="+",
)
args.add_argument(
    "--metric",
    type=str,
)

args = args.parse_args()

core = vapoursynth.core


if args.end == 0:
    args.end = None


def log(message: str, level: str = "info", should_exit: bool = False) -> None:
    """Print message to stderr in JSON format"""
    data = {
        "level": level.upper(),
        "message": message,
        # we may want to display a recoverable error and *not* exit
        "exit": should_exit,
    }

    print(json.dumps(data), file=sys.stderr)


if args.threads != 0:
    core.num_threads = args.threads


class SSIMulacra2:
    def __init__(self) -> None:
        pass

    def calculate(
        self,
        source_video: vapoursynth.VideoNode,
        encoded_video: vapoursynth.VideoNode,
        height: int = None,
        width: int = None,
        skip_frames: int = None,
        start_frame: int = 0,
        end_frame: int = None,
        metric: str = "ssimulacra2_vszip",
    ):
        """Calculate SSIMulacra2 score

        Args:
            self (SSIMulacra2): SSIMulacra2 instance.
            source_video (vapoursynth.VideoNode): Source video.
            encoded_video (vapoursynth.VideoNode): Encoded video to compare.
            height (int, optional): Resized height of video.
            width (int, optional): Resized width of video.
            skip_frames (int, optional): Skip every {INT} of frames. Recommended to be no more than 5 max for accuracy.
            start_frame (int, optional): Start at frame {INT}.
            end_frame (int, optional): End at frame {INT}.
            metric (str, optional): Metric to use.

        Returns:
            index, total_frames, score
        """

        # if both start_frame and end_frame are set
        if start_frame != 0 and end_frame is not None:
            end_frame = end_frame - 1
            source_video = source_video.std.Trim(first=start_frame, last=end_frame)
            encoded_video = encoded_video.std.Trim(first=start_frame, last=end_frame)

        # if only end_frame is set
        if end_frame is not None and start_frame == 0:
            end_frame = end_frame - 1
            source_video = source_video.std.Trim(last=end_frame)
            encoded_video = encoded_video.std.Trim(last=end_frame)

        # if only start_frame is set
        if start_frame != 0 and end_frame is None:
            source_video = source_video.std.Trim(first=start_frame)
            encoded_video = encoded_video.std.Trim(first=start_frame)

        # Skip every N-1 frames if specified and greater than 1
        if skip_frames and skip_frames > 1:
            source_video = source_video.std.SelectEvery(cycle=skip_frames, offsets=0)
            encoded_video = encoded_video.std.SelectEvery(cycle=skip_frames, offsets=0)

        source_video = source_video.resize.Bicubic(
            height=height,
            width=width,
        )

        encoded_video = encoded_video.resize.Bicubic(
            height=height,
            width=width,
        )

        # Slower, CPU based SSIMULACRA2
        if metric == "ssimulacra2_vszip":
            result = source_video.vszip.Metrics(encoded_video, mode=0)
            prop_name = {
                "metric": {
                    "ssimulacra2": [
                        {
                            "method": "ssimu2_vszip",
                            "name": "ssimulacra2",
                            "prop": "_SSIMULACRA2",
                        },
                    ],
                }
            }
        # Faster, GPU accelerated SSIMULACRA2.
        elif metric == "ssimulacra2_vship":
            result = source_video.vship.SSIMULACRA2(encoded_video)
            prop_name = {
                "metric": {
                    "ssimulacra2": [
                        {
                            "method": "ssimu2_vship",
                            "name": "ssimulacra2",
                            "prop": "_SSIMULACRA2",
                        },
                    ],
                }
            }
        elif metric == "butter_vship":
            result = source_video.vship.BUTTERAUGLI(encoded_video)
            prop_name = {
                "metric": {
                    "butteraugli": [
                        {
                            "method": "butter_vship",
                            "name": "2Norm",
                            "prop": "_BUTTERAUGLI_2Norm",
                        },
                        {
                            "method": "butter_vship",
                            "name": "3Norm",
                            "prop": "_BUTTERAUGLI_3Norm",
                        },
                        {
                            "method": "butter_vship",
                            "name": "INFNorm",
                            "prop": "_BUTTERAUGLI_INFNorm",
                        },
                    ],
                }
            }

        for index, frame in enumerate(result.frames()):
            # index is current frame number, result.num_frames is total number of frames
            # score: float = frame.props[prop_name]
            scores = {}
            # go through all the props in prop_name and save the score for each prop to scores
            for metric_category in prop_name.values():
                for metric_name, metric_entries in metric_category.items():
                    for entry in metric_entries:
                        method = entry["method"]
                        name = entry["name"]
                        prop = entry["prop"]

                        if metric_name not in scores:
                            scores[metric_name] = {}
                        if method not in scores[metric_name]:
                            scores[metric_name][method] = {}

                        scores[metric_name][method][name] = frame.props[prop]

            yield {
                "frame": index + 1,
                "total_frames": result.num_frames,
                "score": scores,
            }


def import_video(
    video_path: str, import_method: str, available_import_methods: list, threads: int
) -> vapoursynth.VideoNode:
    """Import the video using the specified import method

    Args:
        video_path (str): The absolute path to the video
        import_method (str): The import method to use
        available_import_methods (list): A list of available import methods
        threads (int): The number of threads to use

    Returns:
        vapoursynth.VideoNode: The imported video as a vapoursynth.VideoNode object
    """

    def try_import(current_method):
        try:
            if current_method == "dgdecnv":
                absolute_dgindex_path = os.path.splitext(video_path)[0] + ".dgi"

                if not os.path.exists(absolute_dgindex_path):
                    command = subprocess.run(
                        [
                            "dgindexnv",
                            "-i",  # video input
                            video_path,
                            "-o",  # index file output
                            absolute_dgindex_path,
                        ],
                        capture_output=True,
                        check=True,
                    )

                return core.dgdecodenv.DGSource(absolute_dgindex_path)

            elif current_method == "ffms2":
                return core.ffms2.Source(video_path)

            elif current_method == "bestsource":
                return core.bs.VideoSource(
                    video_path,
                    threads=threads,
                )

            elif current_method == "lsmash":
                return core.lsmas.LWLibavSource(video_path, threads=threads)
        except Exception as e:
            log(
                f"{os.path.basename(video_path)}: Error with {current_method}: {str(e)}",
                "error",
            )
            return None

    # First try the requested import method
    result = try_import(import_method)
    if result is not None:
        return result

    log(f"Using fallback method", "warning")

    # If the requested method fails, try all available methods in order
    for current_method in available_import_methods:
        if current_method == import_method:
            continue  # Skip the method we already tried

        log(f"Trying {current_method}...", "info")
        result = try_import(current_method)
        if result is not None:
            return result

    # If we exhaust all methods, log error and exit
    log(
        f"Exhausted all available import methods. Unable to import {os.path.basename(video_path)}",
        "error",
        should_exit=True,
    )
    sys.exit(1)


def main() -> None:
    ssimulacra2 = SSIMulacra2()

    available_import_methods: list[str] = []

    for item in args.available_importers:
        available_import_methods.append(item)

    source_vid = import_video(
        video_path=args.source,
        import_method=args.source_importer,
        available_import_methods=available_import_methods,
        threads=args.threads,
    )

    encoded_vid = import_video(
        video_path=args.encoded,
        import_method=args.encoded_importer,
        available_import_methods=available_import_methods,
        threads=args.threads,
    )

    for output in ssimulacra2.calculate(
        source_video=source_vid,
        encoded_video=encoded_vid,
        height=args.height,
        width=args.width,
        skip_frames=args.skip_frames,
        start_frame=args.start,
        end_frame=args.end,
        metric=args.metric,
    ):
        print(json.dumps(output), flush=True)


if __name__ == "__main__":
    main()
