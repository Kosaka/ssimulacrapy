import json
import pathlib
import subprocess
from sys import exit
from queue import Queue
from typing import Generator
from threading import Thread

from rich.progress import Task

from ssimulacrapy.log import logger as log
from ssimulacrapy.options import args
from ssimulacrapy.progress import progress

"""
    Here we can add any new metric we want, without (hopefully)
    having to rewrite the whole thing. So long as every metric's
    output is in the same format, everything should be fine.
"""


class Metrics:
    def __init__(self) -> None:
        self.proc = None
        self.available_importers = []

    def turbo_metrics(
        self,
        source: str,
        encoded: str,
        metric: str = "ssimulacra2",
        every: int = 1,
        start_frame: int = 0,
        end_frame: int = 0,
    ) -> Generator[dict[int, int, float], None, None]:
        """Run turbo metrics on the source and encoded videos

        Args:
            source (str): The absolute path of the source video
            encoded (str): The absolute path of the encoded video
            metric (str): The metric to use
            every (int): The number of frames to skip between metrics
            start_frame (int): The frame to start at
            end_frame (int): The frame to end at

        Returns:
            dict[int, int, float]: frame, total_frames, score
        """

        # Even though we can start at a specific frame, turbo-metrics currently needs to *decode* all the way to that frame
        # Still faster than comparing *and* decoding, but not as fast as it could be.

        process: list[str] = [
            "turbo-metrics",
            "-m",
            metric,
            "--every",
            str(every),
            "--skip",
            str(
                start_frame - 1 if start_frame > 0 else 0
            ),  # skip, *skips* the first x frames, so we need to subtract 1 to start from the correct frame
            "--frames",
            str(end_frame),  # idk if this is even correct.
            "--output",
            "json-lines",
            source,
            encoded,
        ]

        self.proc = subprocess.Popen(
            process, stdout=subprocess.PIPE, stderr=subprocess.PIPE
        )

        frame = start_frame - 1 if start_frame > 0 else 0

        def reader(pipe, queue):
            try:
                with pipe:
                    for line in iter(pipe.readline, b""):
                        queue.put((pipe, line))
            finally:
                queue.put(None)

        q = Queue()
        Thread(target=reader, args=(self.proc.stdout, q)).start()
        Thread(target=reader, args=(self.proc.stderr, q)).start()

        stdout_no = self.proc.stdout.fileno()
        stderr_no = self.proc.stderr.fileno()

        for _ in range(2):
            for source, line in iter(q.get, None):
                output = line.decode("utf-8")
                # stdout
                if source.name == stdout_no:
                    output = json.loads(output)
                    # TODO: Figure out how the hell to get the current frame.

                    if output.get(metric):
                        if isinstance(output[metric], float):
                            score = output[metric]
                            score = {
                                metric: {
                                    args.metric: {
                                        metric: score,
                                    },
                                }
                            }
                            frame += 1
                            yield {
                                "frame": frame,
                                "total_frames": 0,
                                "score": score,
                            }

                # stderr
                elif source.name == stderr_no:
                    if not output.__contains__("INFO"):
                        print(output)

    def ssimulacrapy(
        self,
        source: str,
        encoded: str,
        metric: str,
        every: int,
        start_frame: int,
        end_frame: int,
        source_importer: str,
        encoded_importer: str,
        threads: int,
    ) -> Generator[dict[int, int, float] | dict[str, str, bool], None, None]:
        """Run ssimulacrapy on the source and encoded videos

        Args:
            source (str): The absolute path of the source video
            encoded (str): The absolute path of the encoded video
            metric (str): The metric to use
            every (int): The number of frames to skip between metrics
            start_frame (int): The frame to start at
            end_frame (int): The frame to end at
            source_importer (str): The source video importer
            encoded_importer (str): The encoded video importer

        Returns:
            dict[int, int, float]: frame, total_frames, score, type or dict[str, str, bool]
        """

        # We run the ssimulacrapy_ext vapoursynth script as a separate process.
        # We do this because comparing multiple videos will use a lot of memory and Vapoursynth WILL NOT free it.

        # get the project directory to find the ssimulacrapy_ext.py script
        project_dir = pathlib.Path(__file__).parent

        cmd = [
            "python",
            f"{project_dir}/metrics/ssimulacrapy_ext.py",
            "--source",
            source,
            "--encoded",
            encoded,
            "--metric",
            metric,
            "--skip-frames",
            str(every),
            "--start",
            str(start_frame),
            "--end",
            str(end_frame),
            "--threads",
            str(threads),
            "--source-importer",
            source_importer,
            "--encoded-importer",
            encoded_importer,
            "--available-importers",
            # " ".join(self.available_importers),
        ]

        for item in self.available_importers:
            cmd.append(item)

        self.proc = subprocess.Popen(
            cmd,
            stdout=subprocess.PIPE,
            stderr=subprocess.PIPE,
        )

        def reader(pipe, queue):
            try:
                with pipe:
                    for line in iter(pipe.readline, b""):
                        queue.put((pipe, line))
            finally:
                queue.put(None)

        q = Queue()
        Thread(target=reader, args=(self.proc.stdout, q)).start()
        Thread(target=reader, args=(self.proc.stderr, q)).start()

        stdout_no = self.proc.stdout.fileno()
        stderr_no = self.proc.stderr.fileno()

        for _ in range(2):
            for source, line in iter(q.get, None):
                output = line.decode("utf-8").strip()
                # stdout
                if source.name == stdout_no:
                    try:
                        output = json.loads(output)
                        frame: int = output["frame"]
                        total_frames: int = output["total_frames"]
                        score: dict = output["score"]
                        yield {
                            "frame": frame,
                            "total_frames": total_frames,
                            "score": score,
                        }
                    except json.JSONDecodeError:
                        # we should *only* get json here, assume the worst.
                        # vship will print errors to stdout for example.
                        log.warning(output)
                # stderr
                elif source.name == stderr_no:
                    try:
                        # if we got valid json, return it as it is our own logs
                        # if not, print it and exit as it is something out of our control.
                        yield json.loads(output)
                    except json.JSONDecodeError:
                        print(output)

                        # hide the progress bar when external script fails
                        if self.proc.poll() is not None:
                            prog_tasks: list[Task] = progress.tasks
                            for task in prog_tasks:
                                task.visible = False
                            progress.stop()

                            log.critical(f"Script used for {metric} metric failed.")
                            exit(1)

    def run(
        self,
        method: str,
        metric: str,
        source: str,
        encoded: str,
        source_importer: str,
        encoded_importer: str,
        every: int,
        start_frame: int,
        end_frame: int,
        threads: int,
    ) -> Generator[dict[int, int, float] | dict[str, str, bool], None, None]:
        if method == "ssimulacrapy":
            return self.ssimulacrapy(
                source=source,
                encoded=encoded,
                metric=metric,
                every=every,
                start_frame=start_frame,
                end_frame=end_frame,
                source_importer=source_importer,
                encoded_importer=encoded_importer,
                threads=threads,
            )

        if method == "turbo-metrics":
            return self.turbo_metrics(
                source=source,
                encoded=encoded,
                metric=metric,
                every=every,
                start_frame=start_frame,
                end_frame=end_frame,
            )
