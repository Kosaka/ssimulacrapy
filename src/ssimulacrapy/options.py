import re
import sys
import argparse
from sys import exit
from typing import Dict, ClassVar
from dataclasses import dataclass

from ssimulacrapy._version import __version__

# replace '+' with '.'
__version__ = re.sub(r"\+", ".", __version__)

VALID_IMPORTERS: list[str] = ["dgdecnv", "ffms2", "bestsource", "lsmash"]


@dataclass(frozen=True)
class MetricImplementation:
    """Represents a metric implementation and its relationships"""

    metric_key: str  # e.g. "ssimu2_vszip"
    package: str  # e.g. "ssimulacrapy"
    method_name: str  # e.g. "ssimulacra2_vszip"
    actual_metric: str  # e.g. "ssimulacra2"
    sub_metrics: list[str] | None = None
    _registry: ClassVar[Dict[str, "MetricImplementation"]] = {}

    def __post_init__(self):
        self._registry[self.metric_key] = self

    @classmethod
    def get(cls, metric_key: str) -> "MetricImplementation":
        return cls._registry[metric_key]

    @classmethod
    def get_all_metrics(cls) -> Dict[str, "MetricImplementation"]:
        return cls._registry

    @classmethod
    def validate_metric(cls, metric_key: str) -> bool:
        return metric_key in cls._registry


# Metric configuration declarations
[
    MetricImplementation(
        metric_key="ssimu2_vszip",
        package="ssimulacrapy",
        method_name="ssimulacra2_vszip",
        actual_metric="ssimulacra2",
        sub_metrics=None,
    ),
    MetricImplementation(
        metric_key="ssimu2_vship",
        package="ssimulacrapy",
        method_name="ssimulacra2_vship",
        actual_metric="ssimulacra2",
        sub_metrics=None,
    ),
    MetricImplementation(
        metric_key="butter_vship",
        package="ssimulacrapy",
        method_name="butter_vship",
        actual_metric="butteraugli",
        sub_metrics=["3Norm", "2Norm", "INFNorm"],
    ),
    MetricImplementation(
        metric_key="psnr_turbo",
        package="turbo-metrics",
        method_name="psnr",
        actual_metric="psnr",
        sub_metrics=None,
    ),
    MetricImplementation(
        metric_key="ssim_turbo",
        package="turbo-metrics",
        method_name="ssim",
        actual_metric="ssim",
        sub_metrics=None,
    ),
    MetricImplementation(
        metric_key="msssim_turbo",
        package="turbo-metrics",
        method_name="msssim",
        actual_metric="msssim",
        sub_metrics=None,
    ),
    MetricImplementation(
        metric_key="ssimu2_turbo",
        package="turbo-metrics",
        method_name="ssimulacra2",
        actual_metric="ssimulacra2",
        sub_metrics=None,
    ),
]

# Update valid metrics list
VALID_METRICS = list(MetricImplementation.get_all_metrics().keys())


parser = argparse.ArgumentParser(
    prog="SSIMULACRApy",
    description="Use metrics to score the quality of videos compared to a source.",
)

# Video and score paths
parser.add_argument(
    "--source",
    dest="source",
    type=str,
    help="Source video path. Can be relative to this script or a full path.",
)
parser.add_argument(
    "--encoded",
    dest="encoded",
    type=str,
    nargs="+",
    help="Encoded video path. Can be relative to this script or a full path. Multiple paths can be passed to compare multiple encoded videos.",
)
parser.add_argument(
    "-s",
    "--scores",
    dest="scores",
    type=str,
    help="Scores JSON path. Can be relative to this script or a full path. SSIMULACRApy will save the scores to this path in JSON format.",
)

# Importer options
parser.add_argument(
    "-i",
    "--importer",
    dest="importer",
    choices=VALID_IMPORTERS,
    # default="dgdecnv",
    help="Video importer for the source and encoded videos.",
)
parser.add_argument(
    "-si",
    "--source-importer",
    dest="source_importer",
    choices=VALID_IMPORTERS,
    help="Source video importer. Overrides -i (--importer) for the source video.",
)
parser.add_argument(
    "-ei",
    "--encoded-importer",
    dest="encoded_importer",
    choices=VALID_IMPORTERS,
    help="Encoded video importer. Overrides -i (--importer) for the encoded video.",
)

# select metric
parser.add_argument(
    "-m",
    "--metric",
    dest="metric",
    type=str,
    choices=VALID_METRICS,
    default="ssimu2_vszip",
    help="Metric to use. (default: %(default)s)",
)

# threads to use for Vapoursynth
parser.add_argument(
    "-t",
    "--threads",
    dest="threads",
    type=int,
    default=0,
    help=f"Number of threads to use. Default: 0 (Auto).",
)


# resize video
parser.add_argument(
    "--width", dest="width", type=int, help="Video width to downscale to."
)
parser.add_argument(
    "--height", dest="height", type=int, help="Video height to downscale to."
)

# skip every nth frame
parser.add_argument(
    "-e",
    "--every",
    dest="every",
    type=int,
    default=1,
    help="Frames calculated every nth frame. Default: 1 (Every frame is calculated). For example, setting this to 5 will calculate every 5th frame.",
)

# set frame ranges
parser.add_argument(
    "--start",
    dest="start",
    type=int,
    default=0,
    help="Start frame. Default: 0 (First frame).",
)
parser.add_argument(
    "--end",
    dest="end",
    type=int,
    help="End frame. Default: (Last frame).",
)


# socket client

parser.add_argument(
    "--host",
    dest="host",
    type=str,
    help="Websocket Host & Port. SSIMULACRApy will send frame scores to this host in realtime. Must be in the format ws://{HOST}:{PORT}.",
)

# prints score info periodically
parser.add_argument(
    "--progress",
    dest="progress",
    action="store_true",
    help="Print progress periodically.",
)

# Print out a list of dependencies and whether they are installed
parser.add_argument(
    "--installed",
    dest="installed",
    action="store_true",
    help="Print out a list of dependencies and whether they are installed then exit.",
)


# print version
parser.add_argument("--version", action="version", version="%(prog)s " + __version__)

# prints every frame
parser.add_argument(
    "-v",
    "--verbose",
    dest="verbose",
    action="store_true",
    help="Enable verbose output.",
)

args = parser.parse_args()

if len(sys.argv) == 1:
    parser.print_help()
    exit(1)
