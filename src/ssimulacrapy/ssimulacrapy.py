import os
from sys import exit

from rich import print

from ssimulacrapy.log import logger as log
from ssimulacrapy.utils import b2sum, pprint, calculate_scores
from ssimulacrapy.scores import Scores
from ssimulacrapy.metrics import Metrics
from ssimulacrapy.options import MetricImplementation, args
from ssimulacrapy.progress import progress
from ssimulacrapy.socket_client import SocketClient
from ssimulacrapy.check_requirements import (
    importer,
    source_importer,
    encoded_importer,
    available_importers,
)


def main() -> None:
    scores = Scores(args.scores)
    socket = SocketClient(args.host)

    if not args.source or not args.encoded:
        log.critical("Missing 'Source' or 'Encoded' arguments.")
        exit(1)

    cwd = os.getcwd()

    # Resolve the source video path
    if os.path.isabs(args.source):
        abs_source_path = args.source
    else:
        abs_source_path = os.path.join(cwd, args.source)

    # Resolve the encoded video path(s) and store them in a list
    distorted_files: list = []

    for encoded_file in args.encoded:
        if os.path.isabs(encoded_file):
            _abs_encoded_paths = encoded_file
        else:
            _abs_encoded_paths = os.path.join(cwd, encoded_file)

        distorted_files.append(_abs_encoded_paths)

    # Resolve the scores path
    if args.scores:
        if os.path.isabs(args.scores):
            abs_scores_path = args.scores
        else:
            abs_scores_path = os.path.join(cwd, args.scores)

    # print verbose info
    if args.verbose:
        log.info(f"Source: {abs_source_path}")
        log.info(f"Encoded: {len(distorted_files)}")
        log.info(*distorted_files, sep="\n")
        if args.scores:
            log.info(f"Score file: {abs_scores_path}")
        log.info(f"Source importer: {source_importer}")
        log.info(f"Encoded importer: {encoded_importer}")

    encoded_clip_hash: dict = {}
    task_id: dict = {}
    skipped_file: list = []
    # Import the encoded videos and setup the task_id
    for index, encoded_file in enumerate(distorted_files):
        encoded_filename = os.path.basename(encoded_file)

        # Use only file hash as key
        encoded_clip_hash[encoded_filename] = b2sum(encoded_file)

        if args.scores:
            if os.path.exists(abs_scores_path):
                scores.load()

                # Check if this metric exists for the file
                if scores.exists(
                    os.path.basename(abs_source_path),
                    encoded_clip_hash[encoded_filename],
                    args.metric,
                ):
                    # Skip processing this metric
                    skipped_file.append(index)
                    log.info(f"Skipping {os.path.basename(encoded_file)}...")

                    # get the total frames for current skipped file
                    frames: int = scores.get_frames(
                        os.path.basename(abs_source_path),
                        encoded_clip_hash[encoded_filename],
                    )

                    # get the average score for current skipped file
                    final_scores = scores.get_final_scores(
                        os.path.basename(abs_source_path),
                        encoded_clip_hash[encoded_filename],
                    )

                    metric_impl = MetricImplementation.get(args.metric)
                    avg = (
                        final_scores.get(metric_impl.method_name, {})
                        .get(metric_impl.actual_metric, {})
                        .get(
                            "average",
                        )
                    )

                    # add the task for the skipped file.
                    task_id[encoded_clip_hash[encoded_filename]] = progress.add_task(
                        f"Skipping {index + 1}:",
                        completed=0,
                        total=frames,
                        score=avg,
                    )
                    # update the task to be complete, if we were to set it in add_task() it would be considered active and have the spinner.
                    progress.update(
                        task_id[encoded_clip_hash[encoded_filename]], completed=frames
                    )

                    # stop the task since it's already done.
                    progress.stop_task(task_id[encoded_clip_hash[encoded_filename]])

                else:
                    # Create new task for new metric on existing file
                    task_id[encoded_clip_hash[encoded_filename]] = progress.add_task(
                        f"Calculating File {index + 1}:",
                        score=0,
                        total=None,
                        start=False,
                    )
                    progress.start_task(task_id[encoded_clip_hash[encoded_filename]])
            else:
                # create the progress bar task, set the total to None as we don't know how many frames there are
                # until the first frame is processed.
                task_id[encoded_clip_hash[encoded_filename]] = progress.add_task(
                    f"Calculating File {index + 1}:", score=0, total=None
                )

                progress.start_task(task_id[encoded_clip_hash[encoded_filename]])
        else:
            # create the progress bar task, set the total to None as we don't know how many frames there are
            # until the first frame is processed.
            task_id[encoded_clip_hash[encoded_filename]] = progress.add_task(
                f"Calculating File {index + 1}:", score=0, total=None
            )

            progress.start_task(task_id[encoded_clip_hash[encoded_filename]])

    # remove the skipped files from abs_encoded_path
    if len(skipped_file) > 0:
        for i in skipped_file:
            distorted_files.pop(skipped_file[0])

    # check if there are any files to process
    if len(distorted_files) == 0:
        log.warning("No files to process.")
        exit(0)

    # tell the socket server we are starting
    if args.host:
        socket.start(
            abs_source_path,
            args.start,
            args.end,
            distorted_files,
            encoded_clip_hash,
            args.metric,
        )

    scores.add_video(
        distorted_files,
        encoded_clip_hash,
        os.path.basename(abs_source_path),
        # args.metric,
    )

    # Calculate the scores and do the progress bar
    metric_scores = {}
    with progress:
        # define the scores dict as we're dealing with multiple videos potentially.

        for index, encoded_file in enumerate(distorted_files):
            encoded_filename = os.path.basename(encoded_file)

            metric = Metrics()
            metric.available_importers = available_importers

            if metric_scores.get(encoded_clip_hash[encoded_filename]) is None:
                metric_scores[encoded_clip_hash[encoded_filename]] = {}

            # start the progress task, iirc this should make the timing more accurate?
            progress.start_task(task_id[encoded_clip_hash[encoded_filename]])

            for output in progress.track(
                # calculate the scores
                metric.run(
                    method=MetricImplementation.get(args.metric).package,
                    metric=MetricImplementation.get(args.metric).method_name,
                    source=abs_source_path,
                    encoded=encoded_file,
                    source_importer=(
                        args.source_importer if args.source_importer else importer
                    ),
                    encoded_importer=(
                        args.encoded_importer if args.encoded_importer else importer
                    ),
                    every=args.every,
                    start_frame=args.start,
                    end_frame=0 if args.end is None else args.end,
                    threads=args.threads,
                ),
                task_id=task_id[encoded_clip_hash[encoded_filename]],
            ):
                # check if we've received a message from the metric program
                if output.get("level"):
                    level = output["level"]
                    log.log(level, output["message"])
                    if output["exit"]:
                        exit(1)
                else:
                    frame: int = output["frame"]
                    num_frames: int = output["total_frames"]
                    score: dict = output["score"]

                    # Not all methods return a total number of frames.
                    if num_frames == 0:
                        num_frames = None

                    metric_scores[encoded_clip_hash[encoded_filename]][frame] = score

                    # tell the progress bar the total frames
                    progress.update(
                        task_id[encoded_clip_hash[encoded_filename]],
                        total=num_frames,
                    )

                    if args.verbose:
                        print(f"Frame {frame}/{num_frames}: {score}")

                    if args.host:
                        socket.send_frame(
                            encoded_clip_hash,
                            encoded_file,
                            frame,
                            num_frames,
                            score,
                        )

                    scores.add_frame(
                        os.path.basename(abs_source_path),
                        encoded_clip_hash[encoded_filename],
                        frame,
                        score,
                        # args.metric,
                    )

                    # Calculate average for the progress bar
                    current_scores = scores.get_scores(
                        os.path.basename(abs_source_path),
                        encoded_clip_hash[encoded_filename],
                    )
                    calculated = calculate_scores(current_scores)

                    # Determine which metric and sub-metric to use based on args.metric
                    metric_impl = MetricImplementation.get(args.metric)
                    category = metric_impl.actual_metric

                    average = float(
                        str(
                            calculated[category][args.metric]
                            .get(category, {})
                            .get("average", 69)
                        )[:5]
                    )

                    progress.update(
                        task_id[encoded_clip_hash[encoded_filename]],
                        score=average,
                    )

            # save the scores to dictionary as we're dealing with multiple videos

            calculated = calculate_scores(
                metric_scores[encoded_clip_hash[encoded_filename]].values()
            )

            metric_scores[encoded_clip_hash[encoded_filename]] = calculated
            metric_scores[encoded_clip_hash[encoded_filename]][
                "file"
            ] = encoded_filename

            # send the final scores to the server
            if args.host:
                socket.send_finish(calculated)

            # add the final scores to the scores dict
            scores.finalise_video(
                os.path.basename(abs_source_path),
                encoded_filename,
                encoded_clip_hash,
            )

            # save the scores to the scores file if the user did --scores
            if args.scores:
                scores.save()

            # * Rich's progress bar has an issue where if you add the total to the progress bar -
            # * after it has ended, it resets the speed. There is a MR for this, but it's been closed.
            # * https://github.com/Textualize/rich/pull/2927

            # comp = progress.tasks[
            #     task_id[encoded_clip_hash[encoded_filename]]
            # ].completed

            # progress.update(task_id[encoded_clip_hash[encoded_filename]], total=comp)

    # print the final scores.
    def format_score(value: float) -> float:
        return float(str(value)[:6])

    def print_metric_scores(scores: dict, indent: int = 0) -> None:
        """Recursively print nested metric scores"""
        # when using args.host, there will the the "end" key that is a bool.
        if scores != True:
            for name, value in scores.items():
                if isinstance(value, dict):
                    if (
                        name.lower() != args.metric.lower()
                        and name.lower()
                        != MetricImplementation.get(args.metric).method_name.lower()
                    ):
                        print(f"{' ' * indent}[bold][cyan]{name}[/cyan][/bold]")
                        print_metric_scores(value, indent + 2)
                    else:
                        print_metric_scores(value)
                else:
                    pprint(
                        f"{' ' * indent}[bold]{name.capitalize().replace('_', ' ')}:[/bold][green] {format_score(value)}[/green]"
                    )

    for clip_hash, scores_data in metric_scores.items():
        filename = os.path.basename(scores_data["file"])
        print(f"[bold][yellow]\n{args.metric}: {filename}[/bold][/yellow]")

        # Print all metrics recursively
        for metric_name, metric_values in scores_data.items():
            if metric_name == "file":
                continue
            print_metric_scores(metric_values)
