import os
import shutil
import platform
from sys import exit

from rich import print
from vapoursynth import core

from ssimulacrapy.log import logger as log
from ssimulacrapy.options import args

# TODO: Improve Nuitka support.
# Will need to setup a Docker container to compile the libraries *and* ruin Nuitka in Wine for Windows.

# Get the operating system
sys_os = platform.system().lower()

libraries: dict[str, dict[str, str]] = {
    "dgdecodenv": {
        "windows": "dgdecodenv.dll",
        "linux": "libdgdecodenv.so",
    },
    "ffms2": {"windows": "libffms2.dll", "linux": "libffms2.so"},
    "bs": {
        "windows": "BestSource.dll",
        "linux": "bestsource.so",
    },
    "lsmas": {
        "windows": "lsmas.dll",
        "linux": "liblsmas.so",
    },
    "vszip": {
        "windows": "vszip.dll",
        "linux": "libvszip.so",
    },
    "vship": {"windows": "vship.dll", "linux": "vship.so"},
}

# Check to see if we're compiled by Nuitka
if hasattr(__import__(__name__), "__compiled__"):
    # 'path' is the temp dir the OneFile extracts to but unfortunately it extracts to something akin to "{TEMP}/{RANDOM}/ssimulacrapy/cli"
    # which doesn't exist. We need to go up 2 levels to get the correct path which is what 'new_path' does.
    path = f"{os.path.dirname(__file__)}"
    new_path = "/".join(path.split("/")[:-2])

    # Check dependencies and if we can't load them from the system, load the built-in ones from Nuitka.
    for lib in libraries:
        if not hasattr(core, lib):
            try:
                core.std.LoadPlugin(
                    path=f"{new_path}/vapoursynth/{libraries[lib][sys_os]}"
                )
            except Exception:
                pass

# Check if dependencies are installed

importers = {
    "dgdecodenv": hasattr(core, "dgdecodenv"),
    "ffms2": hasattr(core, "ffms2"),
    "bestsource": hasattr(core, "bs"),
    "lsmas": hasattr(core, "lsmas"),
}
metrics = {
    "vszip": hasattr(core, "vszip"),
    "vship": hasattr(core, "vship"),
    "turbo-metrics": shutil.which("turbo-metrics"),
}

available_importers: list[str] = []
available_metrics: list[str] = []

for _importer in importers:
    if importers[_importer]:
        available_importers.append(_importer)
for _metric in metrics:
    if metrics[_metric]:
        available_metrics.append(_metric)

dgindexnv_installed: str | None = shutil.which("dgindexnv")

global importer, source_importer, encoded_importer

if args.installed:
    """Check if dependencies are installed and print out the results"""

    print("[bold]Installed Plugins:[/bold]")
    print("[bold]   Importers:[/bold]")
    for importer in importers:
        if importer == "dgdecodenv":
            print(
                f"       - {importer}: "
                + (
                    f"[bold][green]Yes[/green][/bold]"
                    if importers[importer]
                    else "[bold][red]No[/red][/bold]"
                )
                + (
                    "[bold][red] (dgindexnv not found in $PATH)[/red][/bold]"
                    if not dgindexnv_installed
                    else ""
                )
            )
        else:
            print(
                f"       - {importer}: "
                + (
                    f"[bold][green]Yes[/green][/bold]"
                    if importers[importer]
                    else "[bold][red]No[/red][/bold]"
                )
            )

    print("[bold]   Metrics:[/bold]")
    for metric in metrics:
        print(
            f"       - {metric}: "
            + (
                f"[bold][green]Yes[/green][/bold]"
                if metrics[metric]
                else "[bold][red]No[/red][/bold]"
            )
        )

    exit(0)


importer: str = args.importer
source_importer: str = args.source_importer
encoded_importer: str = args.encoded_importer


def import_fallback_selector(original_method: str, fallback_method: str) -> None:
    """Set importer, source_importer, and encoded_importer to fallback_method if any of them are set to original_method."""
    if importer == original_method:
        log.warning(f"Falling back to {fallback_method} importer...")
        importer = fallback_method

    if source_importer == original_method:
        log.warning(f"Falling back to {fallback_method} for source importer...")
        source_importer = fallback_method

    if encoded_importer == original_method:
        log.warning(f"Falling back to {fallback_method} for encoded importer...")
        encoded_importer = fallback_method


priority_order: list[str] = ["dgdecodenv", "ffms2", "bestsource", "lsmash"]

for i, current_importer in enumerate(priority_order):
    # Check if any importers are using current method
    if not (
        importer == current_importer
        or source_importer == current_importer
        or encoded_importer == current_importer
    ):
        continue

    # Special case for dgdecodenv
    if current_importer == "dgdecodenv":
        is_available: bool = importers["dgdecodenv"] and dgindexnv_installed
    else:
        is_available: bool = importers[current_importer]

    if is_available:
        continue

    # Find next available fallback
    for fallback_importer in priority_order[i + 1 :]:
        if fallback_importer == "dgdecodenv":
            fallback_available: bool = importers["dgdecodenv"] and dgindexnv_installed
        else:
            fallback_available: bool = importers[fallback_importer]

        if fallback_available:
            if current_importer == "dgdecodenv" and not dgindexnv_installed:
                log.error(
                    "The dgdecodenv plugin is not installed. Please install it and try again."
                )
            log.warning(
                f"{current_importer} not available, falling back to {fallback_importer}..."
            )
            import_fallback_selector(current_importer, fallback_importer)
            break
    else:
        log.error(f"No available fallback importer for {current_importer}")
