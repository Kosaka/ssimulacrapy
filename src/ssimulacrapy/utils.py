import hashlib

from rich.console import Console


# copied from https://stackoverflow.com/questions/65257369/
def b2sum(file: str) -> str:
    """Generate a BLAKE2b hash of a file.

    Args:
        file (str): The file to hash.

    Returns:
        str: The BLAKE2b (digest_size=16) hash of the file.
    """
    with open(file, "rb") as f:
        file_hash = hashlib.blake2b(digest_size=16)
        while chunk := f.read(8192):
            file_hash.update(chunk)
        hexdig: str = file_hash.hexdigest()
        return hexdig


def calculate_scores(
    score_list: list[dict],
) -> dict[str, dict[str, dict[str, float]]]:
    """Calculate scores for nested metric structure"""

    def flatten_metrics(scores, path=None):
        """Recursively flatten nested metric structure"""
        if path is None:
            path = []
        for key, value in scores.items():
            if isinstance(value, dict):
                yield from flatten_metrics(value, path + [key])
            else:
                yield (tuple(path + [key]), value)

    # Collect all metric values with their full paths
    all_metrics = {}
    for frame_scores in score_list:
        for metric_path, value in flatten_metrics(frame_scores):
            if metric_path not in all_metrics:
                all_metrics[metric_path] = []
            all_metrics[metric_path].append(float(str(value)[:5]))

    # Calculate statistics for each metric path
    final_scores = {}
    for metric_path, values in all_metrics.items():
        current = final_scores
        for part in metric_path[:-1]:
            current = current.setdefault(part, {})

        filtered = [v for v in values if v >= 0]
        if not filtered:
            continue

        stats = {
            "average": sum(filtered) / len(filtered),
            "standard_deviation": float(
                str(
                    (
                        sum(x**2 for x in filtered) / len(filtered)
                        - (sum(filtered) / len(filtered)) ** 2
                    )
                    ** 0.5
                )[:5]
            ),
            "median": sorted(filtered)[len(filtered) // 2],
            "percentile_5": (
                sorted(filtered)[len(filtered) // 20]
                if len(filtered) >= 20
                else sorted(filtered)[0]
            ),
            "percentile_95": (
                sorted(filtered)[-len(filtered) // 20]
                if len(filtered) >= 20
                else sorted(filtered)[-1]
            ),
        }

        current[metric_path[-1]] = stats

    # Promote single "value" sub-metrics to the metric level
    for metric in list(final_scores.keys()):
        if isinstance(final_scores[metric], dict) and "value" in final_scores[metric]:
            final_scores[metric] = float(str(final_scores[metric]["value"])[:5])

    return final_scores


def pprint(*args, **kwargs) -> None:
    """Custom ``Rich`` print that with ``highlight=False``."""
    kwargs["highlight"] = False
    console = Console()
    console.print(*args, **kwargs)
