---
hide-toc: true
---

# {{ project }}

**{{ project }}** is a CLI tool for Python that allows you to compare videos using various metrics.


## Features
 - Compare using various metrics: (``SSIMULACRA2``, ``SSIM``, ``MSSSIM``, ``PSNR``)
 - Compare multiple encoded videos with a single source.
 - Multiple choices of importers ``BestSource, Lsmash, DGDecNV``
 - Save scores to JSON for easy analysis
 - Send scores to a websocket server for live analysis
 - Skip frames during comparison for faster comparing
 - Downscale video for faster comparison
 - Ability to start at a specific frame or end at a specific frame

```{toctree}
:hidden:

installation
quickstart
options
```
