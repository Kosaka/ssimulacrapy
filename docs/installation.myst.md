# Installation

## Requirements

  - Importers:
    - [BestSource](https://github.com/vapoursynth/bestsource)
    - [Lsmash](https://github.com/HomeOfAviSynthPlusEvolution/L-SMASH-Works)
    - [DGDecNV](https://www.rationalqm.us/dgdecnv/dgdecnv.html)
```{note}
Only one importer is required, however you can use multiple importers at the same time.

turbo-metrics doesn't require any importers as it handles everything itself.
```

  - Metrics:
    - [Vapoursynth-zip](https://github.com/dnjulek/vapoursynth-zip)
    - [Vapoursynth-HIP-SSIMU2](https://github.com/Line-fr/Vapoursynth-HIP-SSIMU2)
    - [turbo-metrics](https://github.com/Gui-Yom/turbo-metrics)
```{note}
Only one metric is required if you only want to use that one metric.

turbo-metrics supports multiple metrics at the same time, though is NVIDIA only.
```

<!-- - Not in PyPi:
  - [SSIMULACRA2-Zig](https://github.com/dnjulek/vapoursynth-ssimulacra2)
  - Importers:
    - [BestSource](https://github.com/vapoursynth/bestsource)
    - [Lsmash](https://github.com/HomeOfAviSynthPlusEvolution/L-SMASH-Works)
    - [DGDecNV](https://www.rationalqm.us/dgdecnv/dgdecnv.html)
- In PyPi:
- ``vapoursynth rich loguru websocket-client`` -->

## Install
<!-- :::{tab} ArchLinux

Make sure the package `vapoursynth-plugin-ssimulacra2-zig-git` is installed from [codeberg.org/Kosaka/pkgbuilds](https://codeberg.org/Kosaka/pkgbuilds)
Then, either install the package from the [releases](https://codeberg.org/Kosaka/ssimulacrapy/releases) page

```sh
pacman -U ssimulacrapy-*.pkg.tar.zst
```
or build from source using the `PKGBUILD` in the `packaging` directory.

```sh
git clone https://codeberg.org/Kosaka/ssimulacrapy
cd ssimulacrapy/packaging
makepkg -rsi
```
::: -->

:::{tab} Generic Linux/Windows

Make sure you have all required non-PyPi dependencies installed.

Download the wheel from [Releases](https://codeberg.org/kosaka/ssimulacrapy/releases)

**Install via Pipx:**
```sh
pipx install SSIMULACRApy-*.whl
```
:::
