# Configuration file for the Sphinx documentation builder.
#
# For the full list of built-in configuration values, see the documentation:
# https://www.sphinx-doc.org/en/master/usage/configuration.html

# -- Project information -----------------------------------------------------
# https://www.sphinx-doc.org/en/master/usage/configuration.html#project-information

project = "SSIMULACRApy"
# by default Furo uses ``{project} documentation`` for the title.
html_title = f"{project}"
copyright = f"2025, The {project} Devs"
author = "Kosaka"

# -- General configuration ---------------------------------------------------
# https://www.sphinx-doc.org/en/master/usage/configuration.html#general-configuration

extensions = [
    "sphinx_copybutton",
    "sphinx_design",
    "sphinx.ext.coverage",
    "sphinx.ext.intersphinx",
    "sphinx.ext.todo",
    "sphinx.ext.viewcode",
    "sphinx_inline_tabs",
    "myst_parser",
    "sphinx_reredirects",
    "sphinx_tags",
    "sphinx_togglebutton",
    "sphinx_toolbox.decorators",
]

myst_enable_extensions = ["colon_fence", "deflist", "substitution"]

templates_path = ["_templates"]
exclude_patterns = ["_build", "Thumbs.db", ".DS_Store"]

source_suffix = {
    ".rst": "restructuredtext",
    ".myst.md": "markdown",  # The Myst VSCode extension works with .myst.md files only *I think*
    ".md": "markdown",
}

# setup substitutions for rst and myst
rst_epilog = f"""
.. |project| replace:: {project}
"""

myst_substitutions = {
    "project": f"{project}",
}


# -- Options for HTML output -------------------------------------------------
# https://www.sphinx-doc.org/en/master/usage/configuration.html#options-for-html-output

html_theme = "furo"
html_static_path = ["_static"]

html_theme_options = {
    # Theme colours copied and modified from Pyside6 docs.
    "dark_css_variables": {
        "color-brand-primary": "#2cde85",
        "color-brand-content": "#2cde85",
        "color-admonition-title--important": "#2cde85",
        "color-admonition-title-background--important": "#474b53",
        "color-brand-visited": "#23af69",  # for visited links
        "color-highlight-on-target": "#2cde8559",  # stuff like {URL}#info
        "font-stack": "'Titillium Web', sans-serif",
    },
    "light_css_variables": {
        "color-brand-primary": "#24b56d",
        "color-brand-content": "#24b56d",
        "color-admonition-title--important": "#24b56d",
        "color-brand-visited": "#23af69",  # for visited links
        "color-highlight-on-target": "#2cde8559",  # stuff like {URL}#info
        "font-stack": "'Titillium Web', sans-serif",
    },
}
