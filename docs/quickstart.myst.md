# Quickstart

**Compare a source video to an encoded {sup}`(distorted)` video.**
```sh
ssimulacrapy --source source.mkv --encoded encoded.mkv
```
```{tip}
You can compare multiple encoded videos to a single source video by passing a second file to the ``--encoded`` argument.
```sh
ssimulacrapy --source source.mkv --encoded first.mkv second.mkv
```

**Only compare every 5th frame for faster comparisons.**
```{warning}
This should not go higher than 5 for accuracy reasons, ideally you should compare every frame.
```
```sh
ssimulacrapy --source source.mkv --encoded encoded.mkv --every 5
```

**Changing the video importer used.**
```{note}
Currently only ``BestSource, Lsmash, and DGDecNV`` are supported. ``DGDecNV`` is Nvidia only and supports less codecs than the others. Lsmash is known to have issues with some sources but is typically faster than BestSource.
```
```sh
ssimulacrapy -i lsmash --source source.mkv --encoded encoded.mkv
```

**Change the metric used.**
```sh
ssimulacrapy --source source.mkv --encoded encoded.mkv --metric ssimu2_turbo
```

**Downscale video for faster comparison.**
```sh
ssimulacrapy --source source.mkv --encoded encoded.mkv --width 1280 --height 720
```

**Start at a specific frame or end at a specific frame.**
```sh
ssimulacrapy --source source.mkv --encoded encoded.mkv --start 1000 --end 2000
```
```{tip}
Only one is needed, for example if you want to start at frame ``0`` but end at frame ``2000``, use ``--end 2000``.
Same goes for ``--start``.
```sh
ssimulacrapy --source source.mkv --encoded encoded.mkv --end 2000
```

**Send scores to a websocket server for live analysis.**
```sh
ssimulacrapy --source source.mkv --encoded encoded.mkv --host ws://localhost:8080
```

**Save scores to JSON for later analysis.**
```sh
ssimulacrapy --source source.mkv --encoded encoded.mkv --scores scores.json
```
