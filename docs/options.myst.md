# Options

## Info

### `-h, --help`
Show the help message.

### `--installed`
Print status of required dependencies.

### `--version`
Print version and exit.

## Importers

### `-i, --importer {dgdecnv,bestsource,lsmash}`
Video importer for the source and encoded videos.

### `-si`
Video importer for the source video. Overrides the ``--importer`` option for the source video.

### `-ei`
Video importer for the encoded video. Overrides the ``--importer`` option for the encoded video.

## Metrics
### `-m, --metric {ssimu2_vszip,ssimu2_vship,psnr_turbo,ssim_turbo,msssim_turbo,ssimu2_turbo}`
Metric to use for comparison, defaults to ``ssimu2_vszip``.

## Select source and encoded video(s)

### `--source`
Source vide path. Can be relative or a full path.

### `--encoded`
Encoded video path. Can be relative or a full path.

## Outputting scores

### `-s, --scores`
Save scores to file. Can be relative or a full path. SSIMULACRA2 scores will be saved in JSON format.

### `--host`
Send scores to a websocket server in JSON. Must be in the format ``ws://{HOST}:{PORT}``.

## Options during comparison

### `-t, --threads`
Number of threads to use. Default: ``0`` (Auto).

### `--width`
Video width to downscale to.

### `--height`
Video height to downscale to.

### `-e, --every`
Only compare every Nth frame. Default: ``1`` (Every frame is compared).

### `--start`
Start frame. Default: ``0`` (Start at frame 0).

### `--end`
End frame. Default: End at the last frame.

### `--progress`
Print verbose progress periodically to stdout.

### `--verbose`
Enable verbose output.

## Full output of `--help`

:::{admonition} Full output of ``--help``
:class: dropdown

```text
usage: SSIMULACRApy [-h] [--source SOURCE] [--encoded ENCODED [ENCODED ...]] [-s SCORES] [-i {dgdecnv,bestsource,lsmash}] [-si {dgdecnv,bestsource,lsmash}] [-ei {dgdecnv,bestsource,lsmash}]
                    [-m {ssimu2_vszip,ssimu2_vship,psnr_turbo,ssim_turbo,msssim_turbo,ssimu2_turbo}] [-t THREADS] [--width WIDTH] [--height HEIGHT] [-e EVERY] [--start START] [--end END] [--host HOST] [--progress] [--installed]
                    [--version] [-v]

Use metrics to score the quality of videos compared to a source.

options:
  -h, --help            show this help message and exit
  --source SOURCE       Source video path. Can be relative to this script or a full path.
  --encoded ENCODED [ENCODED ...]
                        Encoded video path. Can be relative to this script or a full path. Multiple paths can be passed to compare multiple encoded videos.
  -s, --scores SCORES   Scores JSON path. Can be relative to this script or a full path. SSIMULACRApy will save the scores to this path in JSON format.
  -i, --importer {dgdecnv,bestsource,lsmash}
                        Video importer for the source and encoded videos.
  -si, --source-importer {dgdecnv,bestsource,lsmash}
                        Source video importer. Overrides -i (--importer) for the source video.
  -ei, --encoded-importer {dgdecnv,bestsource,lsmash}
                        Encoded video importer. Overrides -i (--importer) for the encoded video.
  -m, --metric {ssimu2_vszip,ssimu2_vship,psnr_turbo,ssim_turbo,msssim_turbo,ssimu2_turbo}
                        Metric to use. (default: ssimu2_vszip)
  -t, --threads THREADS
                        Number of threads to use. Default: 0 (Auto).
  --width WIDTH         Video width to downscale to.
  --height HEIGHT       Video height to downscale to.
  -e, --every EVERY     Frames calculated every nth frame. Default: 1 (Every frame is calculated). For example, setting this to 5 will calculate every 5th frame.
  --start START         Start frame. Default: 0 (First frame).
  --end END             End frame. Default: (Last frame).
  --host HOST           Websocket Host & Port. SSIMULACRApy will send frame scores to this host in realtime. Must be in the format ws://{HOST}:{PORT}.
  --progress            Print progress periodically.
  --installed           Print out a list of dependencies and whether they are installed then exit.
  --version             show program's version number and exit
  -v, --verbose         Enable verbose output.
```
:::
