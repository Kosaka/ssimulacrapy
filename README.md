# SSIMULACRApy
#### A featureful Python script to compare videos using vapoursynth-zip

1. [Requirements](#requirements)
2. [Installation](#installation)
3. [Usage](#usage)

### Requirements

Any of these metrics plugins are required.

- [vapoursynth-zip](https://github.com/dnjulek/vapoursynth-zip)
- [vapoursynth-hip-ssimu2](https://github.com/Line-fr/Vapoursynth-HIP-SSIMU2)
- [turbo-metrics](https://github.com/Gui-Yom/turbo-metrics)

### Installation

<!-- #### ArchLinux -->

<!-- ##### Manual: -->
<!-- ``` -->
<!-- $ git clone https://codeberg.org/kosaka/ssimulacrapy.git -->
<!-- $ cd ssimulacrapy/packaging -->
<!-- $ makepkg -rsi -->
<!-- ``` -->
<!-- A simpler way would be to download the package from [Releases](https://codeberg.org/kosaka/ssimulacrapy/releases) and install it that way. -->
<!-- ``` -->
<!-- # pacman -U ssimulacrapy-git-*.pkg.tar.zst -->
<!-- ``` -->

#### Linux/Windows

If you're on ArchLinux, you can install some of the metrics through Pacman with makepkg. They can be found in [pkgbuilds](https://codeberg.org/Kosaka/pkgbuilds)

##### Pipx:

Download the wheel from [Releases](https://codeberg.org/kosaka/ssimulacrapy/releases)
```
pipx install SSIMULACRApy-*.whl
```

### Usage

```
usage: SSIMULACRApy [-h] [--source SOURCE] [--encoded ENCODED [ENCODED ...]] [-s SCORES] [-i {dgdecnv,bestsource,lsmash}] [-si {dgdecnv,bestsource,lsmash}] [-ei {dgdecnv,bestsource,lsmash}]
                    [-m {ssimu2_vszip,ssimu2_vship,psnr_turbo,ssim_turbo,msssim_turbo,ssimu2_turbo}] [-t THREADS] [--width WIDTH] [--height HEIGHT] [-e EVERY] [--start START] [--end END] [--host HOST] [--progress] [--installed]
                    [--version] [-v]

Use metrics to score the quality of videos compared to a source.

options:
  -h, --help            show this help message and exit
  --source SOURCE       Source video path. Can be relative to this script or a full path.
  --encoded ENCODED [ENCODED ...]
                        Encoded video path. Can be relative to this script or a full path. Multiple paths can be passed to compare multiple encoded videos.
  -s, --scores SCORES   Scores JSON path. Can be relative to this script or a full path. SSIMULACRApy will save the scores to this path in JSON format.
  -i, --importer {dgdecnv,bestsource,lsmash}
                        Video importer for the source and encoded videos.
  -si, --source-importer {dgdecnv,bestsource,lsmash}
                        Source video importer. Overrides -i (--importer) for the source video.
  -ei, --encoded-importer {dgdecnv,bestsource,lsmash}
                        Encoded video importer. Overrides -i (--importer) for the encoded video.
  -m, --metric {ssimu2_vszip,ssimu2_vship,psnr_turbo,ssim_turbo,msssim_turbo,ssimu2_turbo}
                        Metric to use. (default: ssimu2_vszip)
  -t, --threads THREADS
                        Number of threads to use. Default: 0 (Auto).
  --width WIDTH         Video width to downscale to.
  --height HEIGHT       Video height to downscale to.
  -e, --every EVERY     Frames calculated every nth frame. Default: 1 (Every frame is calculated). For example, setting this to 5 will calculate every 5th frame.
  --start START         Start frame. Default: 0 (First frame).
  --end END             End frame. Default: (Last frame).
  --host HOST           Websocket Host & Port. SSIMULACRApy will send frame scores to this host in realtime. Must be in the format ws://{HOST}:{PORT}.
  --progress            Print progress periodically.
  --installed           Print out a list of dependencies and whether they are installed then exit.
  --version             show program's version number and exit
  -v, --verbose         Enable verbose output.
```

#### Quickstart:
```
$ ssimulacrapy -i bestsource --source source.mkv --encoded encoded.mkv
```
Compare multiple encoded videos to a single source video.:
```
$ ssimulacrapy -i bestsource --source source.mkv --encoded first.mkv second.mkv
```